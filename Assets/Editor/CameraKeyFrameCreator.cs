﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;

[CustomEditor(typeof(Camera))]
public class CameraKeyFrameCreator : Editor
{
    private Object timeline;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Rect rect = EditorGUILayout.GetControlRect(false, 5);
        rect.height = 5;
        EditorGUI.DrawRect(rect, new Color(0.5f, 0.5f, 0.5f, 1));

        timeline = EditorGUILayout.ObjectField(timeline, typeof(Object), true);

        if (GUILayout.Button("Create CameraKeyframe at current values")) {
            if (!this.timeline)
            {
                Debug.LogWarning("Set a timeline reference first!");
                return;
            }

            Camera cam = ((Camera)target);
            GameObject parent = ((GameObject) this.timeline);

            GameObject keyframe = new GameObject();
            keyframe.transform.parent = parent.transform;
            keyframe.name = parent.transform.childCount.ToString();
            keyframe.AddComponent<CameraKeyframe>();
            CameraKeyframe ckf = keyframe.GetComponent<CameraKeyframe>();

            ckf.Position = cam.transform.position;
            ckf.Rotation = cam.transform.rotation.eulerAngles;
            ckf.Size = cam.orthographicSize;
        }
    }
}
