﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CameraKeyframe))]
public class CameraKeyFrameEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        CameraKeyframe script = (CameraKeyframe) target;
        if (GUILayout.Button("Set values from Main Camera"))
        {
            script.Position = Camera.main.gameObject.transform.position;
            script.Rotation = Camera.main.gameObject.transform.rotation.eulerAngles;
            script.Size = Camera.main.orthographicSize;
        }
    }
}
