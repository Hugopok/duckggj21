﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;

public enum Control { Left, Right, Up, Fire }

[System.Serializable]
public class ControlModel
{
    public Transform Control;
    public Control Name;
}

public class ControlPopper : MonoBehaviour
{

    public ControlModel[] Controls;
    public float UIMovementOffset = 10f;
    public float UIMovementDuration = 1.5f;
    public float ControlFireForce = 3f;

    void Start()
    {
        // StartCoroutine(Test());
    }

    IEnumerator Test()
    {
        yield return new WaitForSeconds(3);
        FireControl(Control.Right);
        ControlPickedUp(Control.Up);
        // foreach (ControlModel c in Controls)
        // {
        // FireControl(c.Name);
        // }
    }

    public ControlModel FireRandom()
    {
        Array values = Enum.GetValues(typeof(Control));
        System.Random random = new System.Random();
        Control randomEnum = (Control)values.GetValue(random.Next(values.Length - 1));

        return FireControl(randomEnum);
    }

    public ControlModel FireUp()
    {
        return FireControl(Control.Up);
    }

    public ControlModel FireLeft()
    {
        return FireControl(Control.Left);
    }

    public ControlModel FireRight()
    {
        return FireControl(Control.Right);
    }

    public ControlModel FireBullet()
    {
        return FireControl(Control.Fire);
    }


    public ControlModel FireControl(Control controlName)
    {
        ControlModel cModel = Controls.FirstOrDefault(c => c.Name == controlName);

        Transform uiElement = cModel.Control;

        FireUIElement(uiElement);
        return cModel;
        // FirePrefab(uiElement);
    }

    public void ControlPickedUp(Control controlName)
    {
        Transform uiElement = Controls.FirstOrDefault(c => c.Name == controlName).Control;
        RestoreControl(uiElement);
    }

    public float UIMovementStartDelay = .825f;
    public Ease UIMovementEasing = Ease.InCubic;
    public float UIMovementShakeDuration = .85f;
    public float UIMovementShakeStrength = 7.5f;
    public int UIMovementShakeVibrato = 20;
    public Ease UIMovementShakeEasing = Ease.OutCubic;

    [Range(0f, 1f)] public float UIMovementFireAtPercent = .75f;

    void FireUIElement(Transform uiElement)
    {
        Vector2 target = uiElement.position + (Vector3.left * UIMovementOffset);
        float length = Mathf.Abs(Vector2.Distance(uiElement.position, target));

        bool fired = false;
        uiElement
            .DOMove(target, UIMovementDuration)
            .SetDelay(UIMovementStartDelay)
            .SetEase(UIMovementEasing)
            .OnUpdate(() =>
            {
                if (fired) return;
                float dist = Mathf.Abs(Vector2.Distance(uiElement.position, target));
                if (dist / length <= UIMovementFireAtPercent)
                {
                    fired = true;
                    FirePrefab(uiElement);
                }
            })
            ;

        uiElement
            .DOShakePosition(UIMovementShakeDuration, UIMovementShakeStrength, UIMovementShakeVibrato)
            .SetEase(UIMovementShakeEasing)
            ;
    }

    void FirePrefab(Transform uiElement)
    {
        Vector2 uiWorldPosition = Camera.main.ScreenToWorldPoint(uiElement.position);
        GameObject controlPrefab = uiElement.GetComponent<PrefabReference>().Reference;

        GameObject instance = GameObject.Instantiate(controlPrefab, uiWorldPosition, Quaternion.identity);
        Rigidbody2D rb = instance.GetComponent<Rigidbody2D>();
        rb.AddForce(Vector2.left * ControlFireForce, ForceMode2D.Impulse);
        // rb.AddTorque(5);
    }

    public float RespawnScaleDuration = 1.5f;
    public float RespawnRotationDuration = 1.5f;
    void RestoreControl(Transform uiElement)
    {
        uiElement.localScale = new Vector3(0, 0, 0);
        uiElement.Rotate(new Vector3(0, 0, 30));
        uiElement.position = uiElement.GetComponent<PrefabReference>().StartPosition;
        uiElement.DORotate(Vector3.zero, RespawnRotationDuration).SetEase(Ease.OutElastic);
        var scaleTween = uiElement
            .DOScale(new Vector3(1, 1, 1), RespawnScaleDuration)
            .SetEase(Ease.OutElastic);

        scaleTween.easeOvershootOrAmplitude = 1.25f;
    }

}
