﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderController : MonoBehaviour
{
    public float MinClippingTime = 0.5f;
    public float OverlapCircleRadius = 0.1f;
    public LayerMask RaycastMask;

    private BoxCollider2D col;
    private bool _canStart = false;
    public float waitTime = 0.3f;

    void Start()
    {
        col = this.GetComponent<BoxCollider2D>();

        StartCoroutine(Wait());
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(waitTime);

        _canStart = true;
    }

    void FixedUpdate()
    {
        if (!_canStart) return;

        int overlappedColliders =
            Physics2D.OverlapCircleNonAlloc(transform.position, OverlapCircleRadius, new Collider2D[0], RaycastMask);

        if (overlappedColliders == 0)
        {
            col.enabled = true;
            this.enabled = false;
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, OverlapCircleRadius);
    }
}
