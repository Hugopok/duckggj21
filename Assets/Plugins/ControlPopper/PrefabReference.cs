﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabReference : MonoBehaviour
{
    public GameObject Reference;
    public Vector3 StartPosition;

    void Awake()
    {
        StartPosition = transform.GetComponent<RectTransform>().position;
    }
}
