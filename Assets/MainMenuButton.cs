﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuButton : MonoBehaviour
{
    public void MainMenu()
    {
        SceneManager.LoadSceneAsync("MainMenu",LoadSceneMode.Single);

        Destroy(DuckGameManager.Instance.managersToDestroy,1);
    }
}
