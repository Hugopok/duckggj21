﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuPanel : MonoBehaviour
{
    public GameObject managersToDelete;

    public void Play()
    {
        Destroy(managersToDelete);

        SceneManager.LoadSceneAsync("SampleScene",LoadSceneMode.Single);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
