﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneTrigger : MonoBehaviour
{
    public string KeyframesTag;
    public bool IsTriggered = false;
    public bool ReturnToMenuAfter = false;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (IsTriggered) return;

        var player = collision.gameObject.GetComponent<PlayerController>();

        if (player == null) return;

        if (player.PlayerType == PlayerType.AI)
            return;
        
        CameraMover.Instance.DoAnimation(Camera.main, GameObject.FindWithTag(KeyframesTag), ReturnToMenuAfter);
        IsTriggered = true;
    }
}
