﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeDeathFeedback : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer _spriteRenderer;

    [SerializeField]
    private Sprite _spriteDeath;

    [SerializeField]
    private BoxCollider2D _colliderToIgnore;

    [SerializeField]
    private Rigidbody2D _rb;

    [SerializeField]
    private List<Component> behavioursToDelete;

    public void Death()
    {
        if (_spriteRenderer == null) return;

        if (_rb != null)
            StartCoroutine(WaitRigidbody());

        _spriteRenderer.sprite = _spriteDeath;

        foreach (var b in behavioursToDelete)
        {
            if (b == null) continue;

            Destroy(b);
        }
    }

    private IEnumerator WaitRigidbody()
    {
        yield return _rb.IsSleeping() && _rb.IsTouchingLayers(LayerMask.NameToLayer("Platforms"));

        Destroy(_rb);
        _colliderToIgnore.isTrigger = true;
    }
}
