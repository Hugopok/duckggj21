﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitStopFeedback : AFeedback
{
    [SerializeField]
    private float _duration;
    [SerializeField]
    private bool _destroyOwnerWhenFinished; 

    private bool _isStopped;
    private float _originalTimeScale;
    public override void Play(GameObject owner, object objectToPass)
    {
        if (_isStopped) return;

        transform.parent = null;

        this.owner = owner;

        if(Time.timeScale == 0)
        {
            if (_destroyOwnerWhenFinished)
                Destroy(owner);

            return;
        }

        _originalTimeScale = Time.timeScale;

        playCoroutine = StartCoroutine(PlayCoroutine());
    }

    public override IEnumerator PlayCoroutine()
    {
        Time.timeScale = 0;

        yield return new WaitForSecondsRealtime(_duration);

        Time.timeScale = _originalTimeScale;

        if (_destroyOwnerWhenFinished)
            Destroy(owner);

        Stop();
    }
}
