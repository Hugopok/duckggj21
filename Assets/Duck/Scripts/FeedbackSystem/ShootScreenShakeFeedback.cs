﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;
public class ShootScreenShakeFeedback : AFeedback
{
    [SerializeField]
    private ShakePreset _shakePreset;

    public override void Play(GameObject owner, object objectToPass)
    {
        CameraManager.Instance.CameraShake.Shake(_shakePreset);
    }

    public override IEnumerator PlayCoroutine()
    {
        yield break;
    }
}