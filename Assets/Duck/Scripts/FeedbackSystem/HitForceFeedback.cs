﻿using System.Collections;
using UnityEngine;

public class HitForceFeedback : AFeedback
{
    [SerializeField] protected float impactForce;
    [SerializeField] protected bool applyUpForce;
    [SerializeField] protected ForceMode2D forceToApply;
    [SerializeField]
    protected Rigidbody2D rb;

    public override void Play(GameObject owner, object objectToPass)
    {
        if (rb == null)
            rb = GetComponent<Rigidbody2D>();

        Vector2 direction = (Vector2)objectToPass;

        ApplyHit(impactForce, direction);
    }

    public override IEnumerator PlayCoroutine()
    {
        yield return null;
    }

    protected virtual void ApplyHit(float forceOnHit, Vector3 direction)
    {
        if (rb == null || impactForce <= 0) return;

        rb.velocity = Vector2.zero;

        if (applyUpForce)
            direction = (Vector2)direction + Vector2.up;

        rb.AddForce(direction.normalized * forceOnHit, forceToApply);
    }

}
