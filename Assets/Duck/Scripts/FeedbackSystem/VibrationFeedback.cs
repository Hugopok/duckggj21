﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VibrationFeedback : AFeedback
{
    [SerializeField]
    private float _duration;
    [SerializeField]
    private float _strength;

    public override void Play(GameObject owner, object objectToPass)
    {
        InputManager.Instance.Vibrate(0,_duration,_strength);
    }

    public override IEnumerator PlayCoroutine()
    {
        yield break;
    }
}
