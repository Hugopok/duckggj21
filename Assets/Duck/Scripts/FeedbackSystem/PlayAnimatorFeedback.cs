﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAnimatorFeedback : AFeedback
{
    [SerializeField]
    private GameObject _objectToAnimatePrefab;

    private GameObject _objectAnimateInstantiated;

    [SerializeField]
    private bool _destroyOwnerAtEnd;
    [SerializeField]
    private bool _destroyFeedbackAtEnd;
    [SerializeField]
    private bool _playAloneOnStart;

    private void Start()
    {
        if (_playAloneOnStart)
            Play(gameObject,null);
    }

    public override void Play(GameObject owner, object objectToPass)
    {
        if (_objectToAnimatePrefab == null && !_playAloneOnStart) return;

        transform.parent = null;

        this.owner = owner;

        if (!_playAloneOnStart)
            _objectAnimateInstantiated = Instantiate(_objectToAnimatePrefab, owner.transform.position, Quaternion.identity);
        else
            _objectAnimateInstantiated = gameObject;

        StartCoroutine(PlayCoroutine());
    }

    public override IEnumerator PlayCoroutine()
    {
        var animator = _objectAnimateInstantiated.GetComponent<Animator>();

        while (animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1)
        {
            yield return null;
        }

        Destroy(_objectAnimateInstantiated);

        if (_destroyOwnerAtEnd)
            Destroy(owner);
        if (_destroyFeedbackAtEnd)
            Destroy(gameObject);
    }
}
