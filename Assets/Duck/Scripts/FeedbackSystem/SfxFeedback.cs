﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SfxFeedback : AFeedback
{
    public AudioClip Audio;

    public override void Play(GameObject owner, object objectToPass)
    {
        GameObject obj = new GameObject();
        AudioSource source = obj.AddComponent<AudioSource>();
        source.volume = 0.4f;
        source.PlayOneShot(Audio);
        Destroy(obj, Audio.length + Random.Range(4,10));
    }

    public override IEnumerator PlayCoroutine()
    {
        return null;
    }
}
