﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RabbitEnemySawAction : BaseAction
{
    [SerializeField]
    private Gun _gun;

    [SerializeField]
    private float _alertedTime;

    [Header("Animation Settings")]
    [SerializeField]
    private string _alertedParameter;
    [SerializeField]
    private string _isIdleParameter;

    private bool _isIdle;
    private bool _isAlerted;

    public override void ActionFixedUpdate()
    {
        
    }

    public override void ActionUpdate()
    {
        if (_isAlerted) return;

        _gun?.Shoot();
    }

    protected override void ManageAnimator()
    {
        brain.OwnerAnimator.SetBool(_alertedParameter, _isAlerted);
        brain.OwnerAnimator.SetBool(_isIdleParameter, _isIdle);
    }

    protected override void StateChanged(BrainState state)
    {
        if (state == BrainState.PlayerSaw)
        {
            _isAlerted = true;
            _isIdle = false;

            ManageAnimator();

            StartCoroutine(AlertedRoutine());
        }

        if(state != BrainState.PlayerSaw)
        {
            _gun.StopHeatAI();
        }    
    }

    private IEnumerator AlertedRoutine()
    {
        yield return new WaitForSeconds(_alertedTime);

        _isAlerted = false;
        _isIdle = true;

        ManageAnimator();
    }
}
