﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeAction : BaseAction
{
    [Header("Attack Settings")]
    [SerializeField]
    private float _castDistance;
    [SerializeField]
    private LayerMask _contactLayers;
    [SerializeField]
    private LayerMask _obstacleLayers;
    [SerializeField]
    private float _speed;
    [SerializeField]
    private float _damage;

    [Header("Animation Settings")]
    [SerializeField]
    private string _chargingParameter;

    [Header("Feedbacks")]
    [SerializeField]
    private HitForceFeedback _hitForce;
    [SerializeField]
    private PlayAnimatorFeedback _animatorFeedback;

    private bool _isCharging;

    public override void ActionFixedUpdate()
    {
        transform.Translate(new Vector3(transform.localScale.x, 0, 0) * _speed * Time.deltaTime);

        var hit = Physics2D.Raycast(brain.OwnerController.ColliderCenterPosition,
                                    new Vector3(transform.localScale.x, 0, 0),_castDistance,_contactLayers);

        if (hit.collider == null) return;

        if(hit.collider.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            ApplyDamageToPlayer(hit.collider.gameObject.GetComponent<PlayerController>(),hit);
        }
        else if (LayerUtils.IsInLayerMask(hit.collider.gameObject.layer, _obstacleLayers))
        {
            _hitForce?.Play(gameObject,-hit.normal);

            transform.localScale = new Vector3(transform.localScale.x * -1,1,1);
            //brain.ForceState(BrainState.Idle);
        }
    }

    public override void ActionUpdate()
    {

    }

    protected override void ManageAnimator()
    {
        brain.OwnerAnimator.SetBool(_chargingParameter,_isCharging);
    }

    protected override void StateChanged(BrainState state)
    {
        if (state == BrainState.Idle)
            _isCharging = false;
        else if (state == BrainState.PlayerSaw)
            _isCharging = true;

        ManageAnimator();
    }

    protected virtual void ApplyDamageToPlayer(PlayerController player,RaycastHit2D raycast)
    {
        var dmg = player.gameObject.GetComponent<DamagableComponent>();

        dmg?.Damage(raycast,_damage);
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (brain == null || brain.OwnerController == null) return;

        Debug.DrawRay(brain.OwnerController.ColliderCenterPosition, new Vector3(transform.localScale.x * _castDistance, 0, 0), Color.red);
    }
#endif
}
