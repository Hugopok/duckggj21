﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopInIdleAction : BaseAction
{
    private DamagableComponent _damagableComponent;

    [Header("Animation Settings")]
    [SerializeField]
    private string _idleParameter;

    private bool _isIdle;

    protected override void Start()
    {
        base.Start();

        _damagableComponent = GetComponent<DamagableComponent>();
    }

    public override void ActionFixedUpdate()
    {
        
    }

    public override void ActionUpdate()
    {
        
    }

    protected override void ManageAnimator()
    {
        brain.OwnerAnimator.SetBool(_idleParameter,_isIdle);
    }

    protected override void StateChanged(BrainState state)
    {
        if (state == BrainState.Idle)
        {
            _isIdle = true;
            _damagableComponent.RemoveInvincible();
            StartCoroutine(RoundRoutine());
        }
        else if (state == BrainState.PlayerSaw)
        {
            _isIdle = false;
            _damagableComponent.SetInvincible();
        }

        ManageAnimator();
    }

    private IEnumerator RoundRoutine()
    {
        yield return new WaitForSeconds(0.2f);

        transform.localScale = new Vector3(transform.localScale.x * -1, 1, 1);
    }
}
