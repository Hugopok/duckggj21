﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public enum WalkIdleState
{
    Right,
    Left
}

public class WalkIdleAction : BaseAction
{
    [Header("Loop Walk Settings")]
    [SerializeField]
    private Transform _rightPoint;
    [SerializeField]
    private Transform _leftPoint;
    [SerializeField]
    private float _walkSpeed;
    [SerializeField]
    private float _stopDistance = 0.1f;
    [SerializeField]
    private float _stopToPointPer = 0.4f;
    [SerializeField]
    private WalkIdleState _walkState;

    [Header("Animation Settings")]
    [SerializeField]
    private string _walkParameter;
    [SerializeField]
    private string _idleParameter;

    [Header("Debug")]
    [SerializeField]
    private bool _waitInIdle;
    [SerializeField]
    private Transform _currentPoint;

    protected override void Start()
    {
        if (_walkState == WalkIdleState.Left)
            _currentPoint = _leftPoint;
        else
            _currentPoint = _rightPoint;

        base.Start();
    }

    public override void ActionFixedUpdate()
    {
        if (_waitInIdle) return;

        transform.Translate(new Vector3(transform.localScale.x, 0, 0) * _walkSpeed * Time.deltaTime);
        //Vector3 moveDir = (new Vector3(_currentPoint.position.x,transform.position.y,0) - transform.position).normalized;
        //Vector3 nextFramePos = moveDir * _walkSpeed;
        //brain.OwnerController.RBCharacter.MovePosition(nextFramePos);

    }

    public override void ActionUpdate()
    {
        if (_waitInIdle) return;

        if (CheckDistance(_currentPoint) <= _stopDistance)
        {
            SwapState();

            if (_stopToPointPer > 0)
            {
                _waitInIdle = true;

                StartCoroutine(IdleRoutine());

                return;
            }

            return;
        }

        ManageAnimator();
    }

    protected virtual float CheckDistance(Transform point)
    {
        return Vector2.Distance(new Vector2(transform.position.x, transform.position.y),new Vector2(point.position.x, transform.position.y));
    }

    protected override void StateChanged(BrainState state)
    {
        if (state == BrainState.Hitted)
            _waitInIdle = false;

        if(state == BrainState.Idle)
        {
            float lDistance = CheckDistance(_leftPoint);
            float rDistance = CheckDistance(_rightPoint);

            if (lDistance < rDistance)
                SetState(WalkIdleState.Left);
            else
                SetState(WalkIdleState.Right);
        }

        if (state == BrainState.Idle && _waitInIdle)
        {
            brain.OwnerAnimator.SetBool(_idleParameter, true);
            brain.OwnerAnimator.SetBool(_walkParameter, false);
        }
        else if (state == BrainState.Idle && !_waitInIdle)
        {
            brain.OwnerAnimator.SetBool(_idleParameter, true);
            brain.OwnerAnimator.SetBool(_walkParameter, false);
        }
        else if (state == BrainState.PlayerSaw)
        {
            brain.OwnerAnimator.SetBool(_idleParameter, false);
            brain.OwnerAnimator.SetBool(_walkParameter, false);
        }
    }

    protected virtual void SwapState()
    {
        if (_walkState == WalkIdleState.Left)
        {
            transform.localScale = new Vector3(-1, 1, 1);
            _currentPoint = _leftPoint;
            _walkState = WalkIdleState.Right;
        }
        else
        {
            transform.localScale = new Vector3(1,1,1);
            _currentPoint = _rightPoint;
            _walkState = WalkIdleState.Left;
        }
    }

    protected virtual void SetState(WalkIdleState state)
    {
        if (state == WalkIdleState.Right)
        {
            transform.localScale = new Vector3(-1, 1, 1);
            _currentPoint = _leftPoint;
        }
        else
        {
            transform.localScale = new Vector3(1, 1, 1);
            _currentPoint = _rightPoint;
        }
    }

    protected override void ManageAnimator()
    {
        if (_waitInIdle)
        {
            brain.OwnerAnimator.SetBool(_idleParameter, true);
            brain.OwnerAnimator.SetBool(_walkParameter, false);
        }
        else
        {
            brain.OwnerAnimator.SetBool(_idleParameter, false);
            brain.OwnerAnimator.SetBool(_walkParameter, true);
        }
    }

    protected virtual IEnumerator IdleRoutine()
    {
        ManageAnimator();

        yield return new WaitForSeconds(_stopToPointPer);

        _waitInIdle = false;

        ManageAnimator();
    }
}
