﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseAction : MonoBehaviour
{
    protected Brain brain;

    public bool RunOnUpdate => runOnUpdate;
    public bool RunOnFixedUpdate => runOnFixedUpdate;

    [Header("Common Settings")]
    [SerializeField]
    protected bool runOnFixedUpdate;
    [SerializeField]
    protected bool runOnUpdate;

    protected virtual void Start()
    {
        brain = GetComponent<Brain>();

        brain?.OnStateChanged.AddListener(StateChanged);
    }

    protected abstract void StateChanged(BrainState state);

    public abstract void ActionUpdate();

    public abstract void ActionFixedUpdate();

    protected abstract void ManageAnimator();

}
