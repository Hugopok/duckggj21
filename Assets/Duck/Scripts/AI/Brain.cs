﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public enum BrainState
{
    Idle,
    PlayerSaw,
    Hitted,
    Death
}

public class Brain : BaseAbility
{
    public PlayerController OwnerController => playerController;
    public Animator OwnerAnimator => animator;

    [Header("Event Settings")]
    public BrainStateEvent OnStateChanged;

    [Header("Action Settings")]
    [SerializeField]
    private BaseAction _idleAction;
    [SerializeField]
    private BaseAction _playerSawAction;
    [SerializeField]
    private BaseAction _hittedAction;
    [SerializeField]
    private BaseAction _deathAction;
    [SerializeField]
    private bool _canBeTriggeredInTime;
    [SerializeField]
    private float _triggeredTime;

    [Header("Raycast Settings")]
    [SerializeField]
    private bool _useRaycastLine;
    [SerializeField]
    private bool _useSphereCast;
    [SerializeField]
    private LayerMask _mask;

    [Header("Linecast Settings")]
    [SerializeField]
    private float _distance;

    [Header("Spherecast Settings")]
    [SerializeField]
    private float _radius;

    [Header("Debug")]
    [SerializeField]
    private BrainState _currentState;

    private PlayerController _playerEnemy;
    private BaseAction _currentAction;
    private bool _isTriggered;
    private DamagableComponent damagable;

    protected override void Start()
    {
        base.Start();

        damagable = GetComponent<DamagableComponent>();

        damagable.OnDamage.AddListener(TakeDamage);

        SetState(BrainState.Idle);

        _currentAction = _idleAction;

        if (_useRaycastLine)
            _useSphereCast = false;
        else
            _useRaycastLine = false;
    }

    public virtual void ForceState(BrainState state)
    {
        SetState(state);

        if(_currentState == BrainState.Idle)
        {
            _isTriggered = false;
        }    
    }

    protected override void AbilityFixedUpdate()
    {
        var pTemp = _playerEnemy;

        if (_useRaycastLine)
            _playerEnemy = CheckPlayerWithRaycast();
        else
            _playerEnemy = CheckPlayerWithSphereCast();

        if (_isTriggered && _canBeTriggeredInTime)
            _playerEnemy = pTemp;

        if (_playerEnemy == null)
            SetState(BrainState.Idle);
        else
        {
            SetState(BrainState.PlayerSaw);

            if (_canBeTriggeredInTime && !_isTriggered)
                StartCoroutine(TriggeredRoutine());

            _isTriggered = true;

        }

        if (_currentAction == null) return;

        if (_currentAction.RunOnFixedUpdate)
            _currentAction.ActionFixedUpdate();
    }

    public void TakeDamage(DamagableComponent damagableComponent)
    {
        if (_playerEnemy != null || _currentState == BrainState.PlayerSaw) return;

        _playerEnemy = FindObjectsOfType<PlayerController>().ToList().Find(p => p.PlayerType == PlayerType.Player);

        if (_playerEnemy == null) return;

        Vector3 direction = (transform.position - _playerEnemy.transform.position).normalized;

        if (direction.x > 0)
            transform.localScale = new Vector3(-1, 1, 1);
        else
            transform.localScale = new Vector3(1,1,1);

        SetState(BrainState.PlayerSaw);
    }

    protected override void AbilityUpdate()
    {
        if (_currentAction == null) return;

        if (_currentAction.RunOnUpdate)
            _currentAction.ActionUpdate();
    }

    protected virtual void SetState(BrainState state)
    {
        if (_currentState == state) return;

        _currentState = state;

        switch (_currentState)
        {
            case BrainState.Idle:
                _currentAction = _idleAction;
                break;
            case BrainState.PlayerSaw:
                _currentAction = _playerSawAction;
                break;
            case BrainState.Hitted:
                _currentAction = _hittedAction;
                break;
            case BrainState.Death:
                _currentAction = _deathAction;
                break;
            default:
                break;
        }
        OnStateChanged?.Invoke(_currentState);
    }

    protected virtual PlayerController CheckPlayerWithRaycast()
    {
#if UNITY_EDITOR
        Debug.DrawRay(playerController.ColliderCenterPosition,new Vector3(transform.localScale.x * _distance,0,0),Color.green);
        Debug.DrawRay(playerController.ColliderTopPosition, new Vector3(transform.localScale.x * _distance, 0, 0), Color.green);
        Debug.DrawRay(playerController.ColliderBottomPosition, new Vector3(transform.localScale.x * _distance, 0, 0), Color.green);
#endif

        PlayerController pEnemy = null;

        var hitBottom = Physics2D.Raycast(playerController.ColliderBottomPosition, new Vector3(transform.localScale.x, 0, 0), _distance, _mask);
        var hitCenter = Physics2D.Raycast(playerController.ColliderCenterPosition, new Vector3(transform.localScale.x, 0, 0),_distance, _mask);
        var hitTop = Physics2D.Raycast(playerController.ColliderTopPosition, new Vector3(transform.localScale.x, 0, 0), _distance, _mask);

        if(hitTop.collider != null)
            pEnemy = hitTop.collider.gameObject.GetComponent<PlayerController>();

        if (pEnemy != null) return pEnemy;

        if (hitCenter.collider != null)
            pEnemy = hitCenter.collider.gameObject.GetComponent<PlayerController>();

        if (pEnemy != null) return pEnemy;

        if (hitBottom.collider != null)
            pEnemy = hitBottom.collider.gameObject.GetComponent<PlayerController>();


        return pEnemy;
    }

    protected virtual PlayerController CheckPlayerWithSphereCast()
    {
        var pCollider = Physics2D.OverlapCircle(playerController.ColliderCenterPosition, _radius, _mask);

        if (pCollider == null) return null;

        return pCollider.gameObject.GetComponent<PlayerController>();
    }

    protected virtual IEnumerator TriggeredRoutine()
    {
        yield return new WaitForSeconds(_triggeredTime);

        _isTriggered = false;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        damagable.OnDamage.RemoveListener(TakeDamage);
        OnStateChanged.RemoveAllListeners();
    }
}