﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CameraKeyframe : MonoBehaviour
{
    public Vector3 Position;
    public Vector3 Rotation;
    public float Size;

    public float FrameDuration = 2f;

    // public CameraKeyframe NextKeyframe;

    public bool UseTransitionSettings = false;
    public float TransitionDuration = 1f;
    public Ease TransitionEasing = Ease.InOutQuad;

    public float MovementDuration = 0f;
    public float RotationDuration = 0f;
    public float ZoomDuration = 0f;
    public Ease RotationEasing = Ease.Linear;
    public Ease MovementEasing = Ease.Linear;
    public Ease ZoomEasing = Ease.Linear;

    public bool GravitateTowardsNextKeyframe = false;

    void OnDrawGizmos()
    {
        int index = transform.GetSiblingIndex();
        CameraKeyframe nextKeyframe;

        try {
            nextKeyframe = transform.parent.GetChild(index + 1).GetComponent<CameraKeyframe>();
        } catch {
            return;
        }

        if (!nextKeyframe) return;

        Gizmos.color = Color.red;
        if (GravitateTowardsNextKeyframe)
        {
            Gizmos.color = Color.magenta;
        }
        Gizmos.DrawLine(Position, nextKeyframe.Position);
        Gizmos.DrawSphere(nextKeyframe.Position, .1f);
    }

    public float TimeLength(bool withFrameDuration = true)
    {
        if (this.UseTransitionSettings)
        {
            return (withFrameDuration ? this.FrameDuration : 0) + this.TransitionDuration;
        }
        else
        {
            return (withFrameDuration ? this.FrameDuration : 0) + this.MovementDuration + this.RotationDuration + this.ZoomDuration;
        }
    }

    public float TransitionLength()
    {
        return this.TimeLength(false);
    }
}
