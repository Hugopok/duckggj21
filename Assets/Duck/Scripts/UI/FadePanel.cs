﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;

public class FadePanel : SingletonPrefab<FadePanel>
{
    public float scaleDuration = 0.25f;

    public void DoFadeOpen(Action onCompleted)
    {
        transform.SetParent(FindObjectOfType<Canvas>().transform);

        transform.DOScale(Vector3.zero, scaleDuration).
            SetEase(Ease.Linear).
            OnComplete(() => onCompleted?.Invoke());
    }

    private void OnTransformParentChanged()
    {
        transform.localScale = Vector3.one;
    }
}
