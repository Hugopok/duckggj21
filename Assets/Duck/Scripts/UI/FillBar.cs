﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class FillBar : MonoBehaviour
{
    public Image image;

    private PlayerController player;

    private void Start()
    {
        player = FindObjectsOfType<PlayerController>().ToList().Find(p => p.PlayerType == PlayerType.Player);

        if (player == null) return;

        player.GunAbility.OnHeatTimeChanged += Fill;
    }

    public void Fill(float value)
    {
        image.fillAmount = value / player.GunAbility.MaxConsecutiveFireTime;
    }

    private void OnDestroy()
    {
        player.GunAbility.OnHeatTimeChanged -= Fill;
    }
}
