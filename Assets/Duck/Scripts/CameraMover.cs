﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraMover : Singleton<CameraMover>
{
    public Camera ControlledCamera;
    public GameObject TimelineObject;
    public GameObject Canvas;

    private AudioSource bgm;

    void Start()
    {
        bgm = GameObject.FindWithTag("MainMusic").GetComponent<AudioSource>();
        DoAnimation(Camera.main, GameObject.FindWithTag("IntroKeyframes"));
    }

    void animate()
    {
        var sequence = DOTween.Sequence();
        CameraKeyframe[] keyframes = TimelineObject.GetComponentsInChildren<CameraKeyframe>();
        ControlledCamera.transform.position = keyframes[0].Position;
        ControlledCamera.transform.rotation = Quaternion.Euler(keyframes[0].Rotation);
        ControlledCamera.orthographicSize = keyframes[0].Size;
        
        float timeOffset = keyframes[0].TimeLength();
        CameraKeyframe last = null;
        for (int i = 0; i < keyframes.Length - 1; i++)
        {
            CameraKeyframe keyframe = keyframes[i];
            CameraKeyframe next = keyframes[i + 1];

            // tween position
            float positionTweenDuration = keyframe.UseTransitionSettings ? keyframe.TransitionDuration : keyframe.MovementDuration;
            Ease positionEasing = keyframe.UseTransitionSettings ? keyframe.TransitionEasing : keyframe.MovementEasing;
            sequence.Insert(timeOffset, ControlledCamera.transform
                .DOMove(next.Position, positionTweenDuration)
                .SetEase(positionEasing)
                .OnStart(() => Debug.Log("start tweening position of " + keyframe.gameObject.name))
                // .OnComplete(() => Debug.Log("completed tweening position of " + keyframe.gameObject.name))
            );

            // tween rotation
            float rotationTweenDuration = keyframe.UseTransitionSettings ? keyframe.TransitionDuration : keyframe.RotationDuration;
            Ease rotationEasing = keyframe.UseTransitionSettings ? keyframe.TransitionEasing : keyframe.RotationEasing;
            sequence.Insert(timeOffset, ControlledCamera.transform
                .DORotate(next.Rotation, rotationTweenDuration)
                .SetEase(rotationEasing)
                // .OnStart(() => Debug.Log("start tweening rotation of " + keyframe.gameObject.name))
                // .OnComplete(() => Debug.Log("completed tweening rotation of " + keyframe.gameObject.name))
            );

            // tween camera size
            float scalingTweenDuration = keyframe.UseTransitionSettings ? keyframe.TransitionDuration : keyframe.ZoomDuration;
            Ease scalingEasing = keyframe.UseTransitionSettings ? keyframe.TransitionEasing : keyframe.ZoomEasing;
            sequence.Insert(timeOffset, 
                DOTween.To(
                    () => ControlledCamera.orthographicSize, 
                    value => ControlledCamera.orthographicSize = value,
                    next.Size,
                    scalingTweenDuration
               ).SetEase(scalingEasing)
               // .OnStart(() => Debug.Log("start tweening size of " + keyframe.gameObject.name))
               // .OnComplete(() => Debug.Log("completed tweening size of " + keyframe.gameObject.name))
            );

            if (keyframe.GravitateTowardsNextKeyframe)
            {
                Vector3 direction = (next.Position - keyframe.Position).normalized;
                float gravitationStart = timeOffset + positionTweenDuration;
                Debug.Log("[" + keyframe.gameObject.name + "] gravitation starts at " + gravitationStart + " - duration: " + keyframe.FrameDuration);
                sequence.Insert(gravitationStart, ControlledCamera.transform
                    .DOMove(keyframe.Position + direction * 0.5f, keyframe.FrameDuration)
                    .SetEase(Ease.OutQuad)
                    // .OnStart(() => Debug.Log("start GravitateTowardsNextKeyframe of " + keyframe.gameObject.name))
                    // .OnComplete(() => Debug.Log("completed GravitateTowardsNextKeyframe of " + keyframe.gameObject.name))
                );
            }

            // Debug.Log("[" + keyframe.gameObject.name + "] position tween starts at " + timeOffset + " - duration: " + positionTweenDuration);

            timeOffset += next.TimeLength();
            last = keyframe;
        }

        if (last != null) {
            sequence.AppendInterval(last.FrameDuration);
        }
        
        sequence.Play();
        sequence.OnComplete(this.SequenceCompleted);
    }

    private bool lastWasFinal = false;
    public void DoAnimation(Camera cc, GameObject frames, bool isFinal=false)
    {
        lastWasFinal = isFinal;
        this.ControlledCamera = cc;
        this.TimelineObject = frames;
        Canvas.SetActive(false);
        DuckGameManager.Instance.SetGameState(GameState.InCutscene);
        CameraManager.Instance.DisableProcamera();
        CameraManager.Instance.SetBackgroundColorCutscenes();
        AudioSource audioSource = frames.GetComponent<AudioSource>();
        if (audioSource != null)
        {
            bgm.Pause();
            audioSource.Play();
        }

        animate();
    }

    public void SequenceCompleted()
    {
        if (lastWasFinal)
        {
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
            return;
        }
        DuckGameManager.Instance.SetGameState(GameState.InGame);
        CameraManager.Instance.SetBackgroundColorMain();
        CameraManager.Instance.EnableProcamera();
        Canvas.SetActive(true);
        bgm.Play();
    }
}
