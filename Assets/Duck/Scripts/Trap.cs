﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class Trap : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        var player = collision.gameObject.GetComponent<PlayerController>();

        if (player == null) return;

        if (player.PlayerType == PlayerType.AI)
            Destroy(player.gameObject);
        else
        {
            DuckGameManager.Instance.RespawnToLastCheckPoint();
        }
    }
}
