﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameState
{
    InGame,
    InPause,
    InCutscene,
    InRoomTransition
}

public class DuckGameManager : Singleton<DuckGameManager>
{
    public Action OnUpdate;
    public Action OnFixedUpdate;
    public Action<GameState> OnGameStateChanged;
    public List<Checkpoint> checkpoints = new List<Checkpoint>();
    public Action OnRespawn;

    public GameObject managersToDestroy;

    [Header("Debug")]
    [SerializeField]
    private Checkpoint _currentCheckpoint;

    public GameState CurrentState => _gameState;
    public bool IsPausedWithTransition { get; private set; }
    [SerializeField]
    private GameState _gameState;
    private PlayerController _player;
    public bool isPlayerDead;

    private void Start()
    {
        SceneManager.activeSceneChanged += SceneManager_activeSceneChanged;

        if (SceneManager.GetActiveScene().name == "MainMenu")
            InitializeMainMenu();
        else
            InitializeGameScene();
    }

    private void InitializeMainMenu()
    {
        SetGameState(GameState.InPause);

        InputManager.Instance.ChangeMapToUI();
    }

    private void InitializeGameScene()
    {
        InputManager.Instance.ChangeMapToDefault();

        _player = FindObjectsOfType<PlayerController>().ToList().Find(p => p.PlayerType == PlayerType.Player);

        var l = FindObjectsOfType<Checkpoint>().ToList();

        for (int i = l.Count - 1; i >= 0; i--)
        {
            checkpoints.Add(l[i]);
        }

        SetCheckpoint(checkpoints[0]);

        SetGameState(GameState.InGame);
    }

    private void SceneManager_activeSceneChanged(Scene arg0, Scene arg1)
    {
        if (arg0.name == "MainMenu")
            InitializeMainMenu();
        else
            InitializeGameScene();
    }

    private void Update()
    {
        if (_gameState != GameState.InGame) return;

        OnUpdate?.Invoke();
    }

    private void FixedUpdate()
    {
        if (_gameState != GameState.InGame) return;

        OnFixedUpdate?.Invoke();
    }

    private void OnDestroy()
    {
        OnGameStateChanged = null;
        OnUpdate = null;
        OnFixedUpdate = null;
        OnRespawn = null;
        SceneManager.activeSceneChanged -= SceneManager_activeSceneChanged;
    }

    public void SetCheckpoint(Checkpoint checkpoint)
    {
        _currentCheckpoint = checkpoint;
    }

    public void RespawnToLastCheckPoint()
    {
        isPlayerDead = true;
        _player.Freeze();
        _player.transform.position = _currentCheckpoint.transform.position;
        _player.GetComponent<DamagableComponent>().Revive();
        OnRespawn?.Invoke();
        StartCoroutine(wait());
    }

    private IEnumerator wait()
    {
        yield return new WaitForSeconds(0.2f);

        _player.Unfreeze();
        isPlayerDead = false;
        //SetGameState(GameState.InGame);
    }

    public void SetGameState(GameState state)
    {
        _gameState = state;
        OnGameStateChanged?.Invoke(_gameState);
    }

    public void SwapPause()
    {
        if (_gameState == GameState.InGame)
            _gameState = GameState.InPause;
        else
            _gameState = GameState.InGame;

        OnGameStateChanged?.Invoke(_gameState);
    }

    public void RoomEnter(int prev,int curr)
    {
        if (isPlayerDead) return;
        IsPausedWithTransition = true;

        SetGameState(GameState.InGame);
    }

    public void RoomExit(int prev, int curr)
    {
        if (isPlayerDead) return;

        IsPausedWithTransition = false;

        SetGameState(GameState.InRoomTransition);
    }

}
