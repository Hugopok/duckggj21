﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using System;

public class InputManager : Singleton<InputManager>
{
    #region Constants

    // Actions 
    private const string VERTICAL_MOVEMENT = "Vertical Movement";
    private const string HORIZONTAL_MOVEMENT = "Horizontal Movement";
    private const string VERTICAL_ROTATION = "Vertical Rotation";
    private const string HORIZONTAL_ROTATION = "Horizontal Rotation";
    private const string JUMP = "Jump";
    private const string SHOOT = "Shoot";
    private const string RELOAD = "Reload";
    private const string PAUSE = "Pause";

    #endregion

    #region Privates

    [SerializeField]
    private Rewired.InputManager _rewiredInputManager;

    #endregion

    #region Publics

    public int MaxPlayersCapacity => ReInput.players.playerCount;
    public int DeviceCountConnected => ReInput.controllers.controllerCount;

    public Action<ControllerStatusChangedEventArgs> OnControllerConnected;
    public Action<ControllerStatusChangedEventArgs> OnControllerDisconnected;

    #endregion

    private void Awake()
    {
        _rewiredInputManager = FindObjectOfType<Rewired.InputManager>();

        DontDestroyOnLoad(gameObject);
        DontDestroyOnLoad(_rewiredInputManager.gameObject);

        //if (GameManager.Instance.State == GameState.InGame)
        //{
        //    StartCoroutine(InputManager.Instance.CheckingRoutine("Default", new string[]
        //    {
        //        "Assignament",
        //        "UI"
        //    }));
        //}

        ReInput.ControllerConnectedEvent += ControllerConnectedEvent;
        ReInput.ControllerDisconnectedEvent += ControllerDisconnectedEvent;

        _instance = this;
    }

    public void ChangeMapToDefault()
    {
        StartCoroutine(CheckingRoutine("Default", new string[]
        {
            "UI"
        }));
    }

    public void ChangeMapToUI()
    {
        StartCoroutine(CheckingRoutine("UI", new string[]
        {
            "Default"
        }));
    }

    public IEnumerator CheckingRoutine(string mapToEnable, string[] mapsToDisable)
    {
        while (!ReInput.isReady)
        {
            yield return null;
        }

        if (ReInput.isReady)
        {
            for (int i = 0; i < InputManager.Instance.MaxPlayersCapacity; i++)
            {
                var player = InputManager.Instance.GetPlayerFromID(i);

                if (player == null) continue;

                if (ReInput.controllers.joystickCount > 0)
                    if (i > ReInput.controllers.joystickCount - 1) yield break;

                //if (player.controllers.joystickCount >= 1) continue;

                bool isGamePadConnected = ReInput.controllers.joystickCount > 0 &&
                                          ReInput.controllers.Joysticks[i] != null &&
                                          ReInput.controllers.Joysticks[i].enabled;

                if (i == 0)
                {
                    bool isKeyboardConnected = ReInput.controllers.Keyboard != null &&
                                               ReInput.controllers.Keyboard.enabled;

                    if (isKeyboardConnected)
                        StartCoroutine(InputManager.Instance.AssignmentRoutine(player.id,
                                                                               ReInput.controllers.Keyboard.id,
                                                                               ControllerType.Keyboard,
                                                                               mapToEnable));
                }

                if (isGamePadConnected)
                    StartCoroutine(InputManager.Instance.AssignmentRoutine(player.id,
                                                                           ReInput.controllers.Joysticks[i].id,
                                                                           ReInput.controllers.Joysticks[i].type,
                                                                           mapToEnable));

                foreach (var mapName in mapsToDisable)
                {
                    player.controllers.maps.SetMapsEnabled(false, mapName);
                }
            }
        }
    }

    public IEnumerator AssignmentRoutine(int playerid, int controllerid, ControllerType controllerType, string mapName)
    {
        while (!ReInput.isReady)
        {
            yield return null;
        }

        yield return new WaitForEndOfFrame();

        AssignController(playerid, controllerid, controllerType, mapName);
    }

    //Events

    private void ControllerDisconnectedEvent(ControllerStatusChangedEventArgs obj)
    {
        OnControllerDisconnected?.Invoke(obj);
        ControllerDisconnected(obj);
    }

    private void ControllerConnectedEvent(ControllerStatusChangedEventArgs obj)
    {
        ControllerConnected(obj);
        OnControllerConnected?.Invoke(obj);
    }

    private void ControllerConnected(ControllerStatusChangedEventArgs obj)
    {
        Debug.Log($"Controller connected {obj.controllerId}  {obj.controllerType}  {obj.name}");

        var rewiredPlayer = ReInput.players.GetPlayer(obj.controllerId);

        AssignController(rewiredPlayer.id, obj.controller.id, obj.controllerType);
    }

    private void ControllerDisconnected(ControllerStatusChangedEventArgs obj)
    {
        Debug.Log($"Controller disconnected {obj.controllerId}  {obj.controllerType}  {obj.name}");

        RemoveController(obj.controllerId, obj.controllerId, obj.controllerType);
    }

    // Axis
    public Vector2 GetInputVector(int playerId)
    {
        if (!IsPlayerValid(playerId)) return Vector2.zero;

        return GetPlayerFromID(playerId).GetAxis2D(HORIZONTAL_MOVEMENT, VERTICAL_MOVEMENT);
    }

    public Vector2 GetInputVectorRaw(int playerId)
    {
        if (!IsPlayerValid(playerId)) return Vector2.zero;

        return GetPlayerFromID(playerId).GetAxis2DRaw(HORIZONTAL_MOVEMENT, VERTICAL_MOVEMENT);
    }

    public float GetVerticalMovement(int playerId)
    {
        if (!IsPlayerValid(playerId)) return 0;

        return GetPlayerFromID(playerId).GetAxis(VERTICAL_MOVEMENT);
    }

    public float GetVerticalMovementRaw(int playerId)
    {
        if (!IsPlayerValid(playerId)) return 0;

        return GetPlayerFromID(playerId).GetAxisRaw(VERTICAL_MOVEMENT);
    }

    public float GetHorizontalMovement(int playerId)
    {
        if (!IsPlayerValid(playerId)) return 0;

        return GetPlayerFromID(playerId).GetAxis(HORIZONTAL_MOVEMENT);
    }

    public float GetHorizontalMovementRaw(int playerId)
    {
        if (!IsPlayerValid(playerId)) return 0;

        return GetPlayerFromID(playerId).GetAxisRaw(HORIZONTAL_MOVEMENT);
    }

    public Vector2 GetRotationVector(int playerId)
    {
        if (!IsPlayerValid(playerId)) return Vector2.zero;

        return GetPlayerFromID(playerId).GetAxis2D(HORIZONTAL_ROTATION, VERTICAL_ROTATION);
    }

    public Vector2 GetRotationVectorRaw(int playerId)
    {
        if (!IsPlayerValid(playerId)) return Vector2.zero;

        return GetPlayerFromID(playerId).GetAxis2DRaw(HORIZONTAL_ROTATION, VERTICAL_ROTATION);
    }

    public Vector2 GetMousePosition(int playerId)
    {
        if (!IsPlayerValid(playerId)) return Vector2.zero;

        Player p = GetPlayerFromID(playerId);
        if (!p.controllers.hasMouse)
            return Vector2.zero;

        return p.controllers.Mouse.screenPosition;
    }

    //Pause
    public bool IsPauseButtonPressed(int playerId)
    {
        if (!IsPlayerValid(playerId)) return false;

        return GetPlayerFromID(playerId).GetButtonDown(PAUSE);
    }

    //Jump
    public bool IsJumpButtonPressed(int playerId)
    {
        if (!IsPlayerValid(playerId)) return false;

        return GetPlayerFromID(playerId).GetButtonDown(JUMP);
    }

    public bool IsJumpButtonHold(int playerId)
    {
        if (!IsPlayerValid(playerId)) return false;

        return GetPlayerFromID(playerId).GetButton(JUMP);
    }

    public bool IsJumpButtonReleased(int playerId)
    {
        if (!IsPlayerValid(playerId)) return false;

        return GetPlayerFromID(playerId).GetButtonUp(JUMP);
    }

    //Shooting
    public bool IsFireButtonPressed(int playerId)
    {
        if (!IsPlayerValid(playerId)) return false;

        return GetPlayerFromID(playerId).GetButtonDown(SHOOT);
    }

    public bool IsFireButtonHold(int playerId)
    {
        if (!IsPlayerValid(playerId)) return false;

        return GetPlayerFromID(playerId).GetButton(SHOOT);
    }

    public bool IsFireButtonReleased(int playerId)
    {
        if (!IsPlayerValid(playerId)) return false;

        return GetPlayerFromID(playerId).GetButtonUp(SHOOT);
    }

    //Reload
    public bool IsReloadButtonPressed(int playerId)
    {
        if (!IsPlayerValid(playerId)) return false;
        return GetPlayerFromID(playerId).GetButtonDown(RELOAD);
    }

    public bool IsReloadButtonHold(int playerId)
    {
        if (!IsPlayerValid(playerId)) return false;

        return GetPlayerFromID(playerId).GetButton(RELOAD);
    }

    public bool IsReloadButtonReleased(int playerId)
    {
        if (!IsPlayerValid(playerId)) return false;

        return GetPlayerFromID(playerId).GetButtonUp(RELOAD);
    }

    //Utils
    public bool IsPlayerUsingKeyboard(int playerId)
    {
        if (!IsPlayerValid(playerId)) return false;

        Player p = GetPlayerFromID(playerId);
        // Get the last active controller the Player was using
        Controller activeController = p.controllers.GetLastActiveController();

        if (activeController == null)
        {
            // player hasn't used any controllers yet
            // No active controller, set a default
            if (p.controllers.joystickCount > 0)
            {
                // try to get the first joystick in player
                activeController = p.controllers.Joysticks[0];
            }
            else
            { // no joysticks assigned, just get keyboard
                activeController = p.controllers.Keyboard;
            }
        }

        return activeController.type == ControllerType.Keyboard || activeController.type == ControllerType.Mouse;
    }

    public void EnableMap(int playerId, bool enable, string mapName)
    {
        if (!IsPlayerValid(playerId)) return;

        var p = GetPlayerFromID(playerId);

        p.controllers.maps.SetMapsEnabled(enable, mapName);
    }

    public bool IsAtLeastOneGamepadConnected()
    {
        foreach (var item in ReInput.controllers.Controllers)
        {
            if (!item.hardwareName.Equals("Keyboard") || !item.hardwareName.Equals("Mouse"))
                return true;
        }

        return false;
    }

    public Player GetPlayerFromID(int playerId)
    {
        return ReInput.players.GetPlayer(playerId);
    }

    public Player GetPlayerFromController(ControllerType controllerType, int controllerId)
    {
        foreach (var p in ReInput.players.GetPlayers())
        {
            if (p == null) continue;

            var c = p.controllers.GetController(controllerType, controllerId);

            if (c == null) continue;

            return p;
        }

        return null;
    }

    public void AssignController(int id, int controllerId, ControllerType controllerType, string mapName = "Assignament")
    {
        var p = GetPlayerFromID(id);

        p.controllers.AddController(controllerType, controllerId, true);

        p.controllers.maps.SetMapsEnabled(true, mapName);

#if UNITY_EDITOR

        Debug.Log($"Controller assigned to player_{id} controller : {controllerType}");

        string hardwareName = "Keyboard";
        if (controllerType == ControllerType.Joystick)
            hardwareName = p.controllers.Joysticks[0].hardwareName;

        Debug.Log($"HADRWARE NAME : {hardwareName}");
#endif
    }

    public void RemoveController(int id, int controllerId, ControllerType controllerType, string mapName = "MainMenu")
    {
        var p = GetPlayerFromID(id);

        p.controllers.RemoveController(controllerType, controllerId);

        p.controllers.maps.SetMapsEnabled(false, mapName);
    }

    public bool IsUISubmitPressed()
    {
        var p = GetPlayerFromID(0);

        if (p == null) return false;

        return p.GetButtonDown("UISubmit");
    }

    public bool IsUICancelPressed()
    {
        var p = GetPlayerFromID(0);

        if (p == null) return false;

        return p.GetButtonDown("UICancel");
    }

    private bool IsPlayerValid(int playerId)
    {
        return GetPlayerFromID(playerId) != null;
    }

    public void Vibrate(int playerId,float duration, float strength)
    {
        var player = GetPlayerFromID(playerId);

        if (player == null) return;

        if(player.controllers.joystickCount <= 0)
        {
#if UNITY_EDITOR
            Debug.LogWarning($"Player with id : {playerId} has no controller");
#endif
            return;
        }

        player.SetVibration(0,strength,duration);
    }
}