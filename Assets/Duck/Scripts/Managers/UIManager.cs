﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired.UI;
using Rewired.Integration.UnityUI;

public class UIManager : Singleton<UIManager>
{
    public GameObject pausePanel;
    public GameObject buttonToSelect;
    public RewiredEventSystem uiSystem;

    private void Awake()
    {
        DuckGameManager.Instance.OnGameStateChanged += PauseActivated;
    }

    void PauseActivated(GameState state)
    {
        if (pausePanel == null) return;

        if (state == GameState.InPause)
        {
            pausePanel.SetActive(true);

            SelectButton();
        }
        else
            pausePanel.SetActive(false);
    }

    public void Play()
    {
        DuckGameManager.Instance.SwapPause();
    }

    public void Quit()
    {
        Application.Quit();
    }

    void SelectButton() 
    {
        uiSystem.SetSelectedGameObject(buttonToSelect);
        uiSystem.firstSelectedGameObject = buttonToSelect;
    }

}
