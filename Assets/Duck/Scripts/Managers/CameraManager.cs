﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;

public class CameraManager : Singleton<CameraManager>
{
    public ProCamera2D ProCamera2D => _proCamera;
    public ProCamera2DShake CameraShake => _shake;

    [SerializeField]
    private ProCamera2D _proCamera;

    private ProCamera2DShake _shake;
    public ProCamera2DRooms _rooms;
    private void Start()
    {
        if (_proCamera == null)
            _proCamera = FindObjectOfType<ProCamera2D>();

        _shake = ProCamera2D.GetComponent<ProCamera2DShake>();
        _rooms = ProCamera2D.GetComponent<ProCamera2DRooms>();

        DuckGameManager.Instance.OnRespawn += DisableRoomTransition;
    }

    public void SetBackgroundColorMain()
    {
        Camera.main.backgroundColor = Color.black;
    }

    public void SetBackgroundColorCutscenes()
    {
        Camera.main.backgroundColor = new Color(36f/255, 36f/255, 36f/255);
    }

    void DisableRoomTransition()
    {
        StartCoroutine(wait());
    }

    public void DisableProcamera()
    {
        _proCamera.enabled = false;
    }

    public void EnableProcamera()
    {
        _proCamera.enabled = true;
    }

    IEnumerator wait()
    {
        List<float> trans = new List<float>();

        foreach (var item in _rooms.Rooms)
        {
            trans.Add(item.TransitionDuration);
            item.TransitionDuration = 0;
        } 

        yield return new WaitForSeconds(0.1f);

        for (int i = 0; i < _rooms.Rooms.Count; i++)
        {
            _rooms.Rooms[i].TransitionDuration = trans[i];
        }
    }
}
