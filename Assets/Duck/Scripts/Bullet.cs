﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Vector3 Direction => _direction;

    public float Damage;
    public Transform castPosition;
    public float bulletSpeed;
    public LayerMask contactLayer;
    public float radius;
    public Rigidbody2D rb;

    [Header("Feedbacks")]
    public PlayAnimatorFeedback destroyFeedback;
    public HitStopFeedback hitStopFeedback;

    private bool _isObjectHitted;
    private Vector3 prevPos;
    private Vector3 _direction;
    private bool _isInitialized = false;
    private BoxCollider2D _collider;

    private Vector2 _velocityBeforePause;

    public void Initialize(Vector3 dir,float gunZRotation)
    {
        _collider = GetComponent<BoxCollider2D>();

        if (dir.x == -1)
            gunZRotation = -180;

        float spread = Random.Range(-5,10);
        Quaternion bulletRotation = Quaternion.Euler(new Vector3(0, 0, gunZRotation + spread));

        transform.rotation = bulletRotation;
        _isInitialized = true;
        _direction = dir;

        DuckGameManager.Instance.OnFixedUpdate += CustomCollider;
        DuckGameManager.Instance.OnUpdate += CheckPrevPosition;
        DuckGameManager.Instance.OnGameStateChanged += StateChanged;

        Debug.DrawRay(transform.position, dir * bulletSpeed, Color.yellow);
        
        rb.AddForce(dir.normalized * bulletSpeed,ForceMode2D.Impulse);

        Destroy(gameObject,10);
    }

    protected virtual void StateChanged(GameState state)
    {
        if (state == GameState.InPause || state == GameState.InRoomTransition)
        {
            _isInitialized = false;

            _velocityBeforePause = rb.velocity;

            rb.velocity = Vector2.zero;
        }
        else
        {
            _isInitialized = true;

            rb.velocity = _velocityBeforePause;
        }
    }

    protected virtual void CustomCollider()
    {
        if (!_isInitialized) return;
        if (_isObjectHitted) return;

        var col = Physics2D.OverlapCircle(castPosition.position, radius, contactLayer);

        if (col == null || col.gameObject == null || col.gameObject == gameObject) return;

        if (LayerUtils.IsInLayerMask(col.gameObject.layer, contactLayer))
        {
            Vector3 rayStart = col.ClosestPoint((Vector2)prevPos);

            RaycastHit2D hit;

            hit = Physics2D.Raycast(rayStart, transform.right,0.5f);

            if (hit.collider != null)
            {
                if (CheckIfIsAlreadyDeath(hit.collider.gameObject)) return;

                if (LayerUtils.IsInLayerMask(hit.collider.gameObject.layer, contactLayer))
                {
#if UNITY_EDITOR
                    Debug.DrawRay(rayStart, transform.right, Color.blue);
#endif
                    _isObjectHitted = true;
                    PerformContact(col, hit);
                }
            }
        }
    }

    private bool CheckIfIsAlreadyDeath(GameObject hitted)
    {
        var dmg = hitted.GetComponent<DamagableComponent>();

        if (dmg == null) return false;

        return dmg.IsDeath;
    }

    private void PerformContact(Collider2D col, RaycastHit2D hit)
    {
        var damagableComponent = col.gameObject.GetComponent<DamagableComponent>();

        if (damagableComponent != null)
        {
            damagableComponent?.Damage(hit, Damage);
            hitStopFeedback?.Play(gameObject,null);
        }

        destroyFeedback?.Play(gameObject, null);

        gameObject.SetActive(false);
    }

    private void CheckPrevPosition()
    {
        if (!_isInitialized || _isObjectHitted) return;

        prevPos = transform.position;
    }

    private void OnDisable()
    {
        DuckGameManager.Instance.OnFixedUpdate -= CustomCollider;
        DuckGameManager.Instance.OnUpdate -= CheckPrevPosition;
    }

    private void OnDestroy()
    {
        DuckGameManager.Instance.OnFixedUpdate -= CustomCollider;
        DuckGameManager.Instance.OnUpdate -= CheckPrevPosition;
        DuckGameManager.Instance.OnGameStateChanged -= StateChanged;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawSphere(castPosition.position, radius);
    }
#endif
}
