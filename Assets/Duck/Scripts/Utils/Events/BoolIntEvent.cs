﻿using UnityEngine.Events;

[System.Serializable]
public class BoolIntEvent : UnityEvent<bool, int> { }
