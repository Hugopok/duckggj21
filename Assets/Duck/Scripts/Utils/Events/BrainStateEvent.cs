﻿using System;
using UnityEngine.Events;
using UnityEngine;

[Serializable]
public class BrainStateEvent : UnityEvent<BrainState> { }
