﻿using System;
using UnityEngine.Events;
using UnityEngine;

[Serializable]
public class ObjectMoveEvent : UnityEvent<Vector3> { }