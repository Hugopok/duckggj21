﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DropAndGainCommands : BaseAbility
{
    [SerializeField]
    private Gun _gun;
    [SerializeField]
    private HorizontalMove _hMove;
    [SerializeField]
    private Jump _jump;

    public float gainRadius;

    public ControlPopper controlPopper;
    public LayerMask contacts;

    private DamagableComponent damagable;

    public void RoomEnter(int currentRoom, int x)
    {
        Respawned();
    }

    protected override void Start()
    {
        base.Start();

        CameraManager.Instance._rooms.OnStartedTransition.AddListener(RoomEnter);

        if (controlPopper == null)
            controlPopper = FindObjectOfType<ControlPopper>();

        DuckGameManager.Instance.OnRespawn += Respawned;
    }

    void Respawned()
    {
        if (damagable == null)
        {
            damagable = GetComponent<DamagableComponent>();
        };

        StartCoroutine(wait());
    }

    private IEnumerator wait()
    {
        yield return new WaitForSeconds(1.5f);

        GainAbility(Control.Right);
        GainAbility(Control.Left);
        GainAbility(Control.Up);
        GainAbility(Control.Fire);

        var buttonFire = FindObjectsOfType<NewButtonBehaviour>().ToList().Find(b => b.control == Control.Fire);
        var buttonRight = FindObjectsOfType<NewButtonBehaviour>().ToList().Find(b => b.control == Control.Right);
        var buttonLeft = FindObjectsOfType<NewButtonBehaviour>().ToList().Find(b => b.control == Control.Left);
        var buttonUp = FindObjectsOfType<NewButtonBehaviour>().ToList().Find(b => b.control == Control.Up);

        if (buttonRight != null)
            Destroy(buttonRight.gameObject);

        if (buttonLeft != null)
            Destroy(buttonLeft.gameObject);

        if (buttonFire != null)
            Destroy(buttonFire.gameObject);

        if (buttonUp != null)
            Destroy(buttonUp.gameObject);
    }

    public void DamageEvent(DamagableComponent damagableComponent)
    {
        if (damagable == null) damagable = damagableComponent;

        if (controlPopper == null) return;

        ControlModel cModel = null;

        if (damagable.Health == 0)
        {
            cModel = controlPopper.FireControl(Control.Fire);

            DuckGameManager.Instance.RespawnToLastCheckPoint();

            return;
        }
        else if(damagable.Health == 1)
            cModel = controlPopper.FireControl(Control.Right);
        else if (damagable.Health == 2)
            cModel = controlPopper.FireControl(Control.Up);
        else if (damagable.Health == 3)
            cModel = controlPopper.FireControl(Control.Left);

        DisableAbility(cModel.Name);
    }

    public void GainAbility(Control control)
    {
        if (controlPopper == null) return;

        controlPopper.ControlPickedUp(control);

        AllowAbility(control);
    }

    protected void AllowAbility(Control control)
    {
        switch (control)
        {
            case Control.Left:
                _hMove.AllowDirection(MoveDirection.Left);
                break;
            case Control.Right:
                _hMove.AllowDirection(MoveDirection.Right);
                break;
            case Control.Up:
                _jump.ForceStopAbility = false;
                break;
            case Control.Fire:
                _gun.ForceStopAbility = false;
                break;
            default:
                break;
        }
    }

    protected void DisableAbility(Control control)
    { 
        switch (control)
        {
            case Control.Left:
                _hMove.DisallowDirection(MoveDirection.Left);
                break;
            case Control.Right:
                _hMove.DisallowDirection(MoveDirection.Right);
                break;
            case Control.Up:
                _jump.ForceStopAbility = true;
                break;
            case Control.Fire:
                _gun.ForceStopAbility = true;
                break;
            default:
                break;
        }
    }

    protected override void AbilityFixedUpdate()
    {
        //int layer = LayerMask.NameToLayer("Buttons");
        var col = Physics2D.OverlapCircle(transform.position,gainRadius, contacts);

        if(col != null && LayerUtils.IsInLayerMask(col.gameObject.layer, contacts))
        {
            var buttonBh = col.gameObject.GetComponent<NewButtonBehaviour>();

            if (buttonBh == null) return;

            GainAbility(buttonBh.control);

            damagable.AddHealth(1);

            Destroy(buttonBh.gameObject);
        }
    }

    protected override void AbilityUpdate()
    {

    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position,gainRadius);
    }
#endif
}
