﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MoveDirection
{
    Left,
    Right
}

[System.Serializable]
public class DirectionModel
{
    public MoveDirection permittedDirection;
    public bool canMove;
}

public class HorizontalMove : BaseAbility
{
    public Vector2 VelocityVector => _velocityVector;
    public Vector2 InputVector => _inputVector;
    public bool ForceDown => (CheckWall() && !playerController.IsGrounded &&
         ((!playerController.AbilityJump.IsJumping && 
           !playerController.GunAbility.IsFiring && 
           !playerController.AbilityJump.CanPerformCoyote) || 
           (inputManager.IsJumpButtonHold(0) && playerController.AbilityJump.IsJumping)));

    public BoolEvent OnFlipX;

    [Header("Movement Settings")]
    [SerializeField]
    private float _speed;
    [SerializeField]
    private List<DirectionModel> _permittedDirection;
    
    [Header("Wall Checker")]
    [SerializeField]
    private float _castDistanceChecker;

    [Header("Animations")]
    [SerializeField]
    private string _movementParameter;
    [SerializeField]
    private string _upParameter;
    [SerializeField]
    private string _downParameter;

    private Vector2 _inputVector;
    private Vector2 _velocityVector;
    private Vector2 velTemp;

    protected override void Start()
    {
        base.Start();

        _permittedDirection = new List<DirectionModel>()
        {
            new DirectionModel() { canMove = true, permittedDirection = MoveDirection.Left },
            new DirectionModel() { canMove = true, permittedDirection = MoveDirection.Right}
        };
    }

    protected override void AbilityFixedUpdate()
    {
        if (!CanPerformAbility) return;

        if (ForceDown)
        {
            forceDown = true;
            playerController.RBCharacter.velocity = new Vector2(0,playerController.RBCharacter.velocity.y);
            return;
        }

        forceDown = false;

        if (_velocityVector.x == 0 || playerController.GunAbility.IsFiring) return;

        playerController.RBCharacter.velocity = new Vector2(_velocityVector.x * _speed * Time.deltaTime,playerController.RBCharacter.velocity.y);
    }

    public bool forceDown; 

    protected override void AbilityUpdate()
    {
        if (!CanPerformAbility) return;

        velTemp = _velocityVector;

        _inputVector = inputManager.GetInputVectorRaw(0);

        _velocityVector = new Vector2(_inputVector.x, playerController.RBCharacter.velocity.y);

        var left = GetDirectionModel(MoveDirection.Left);
        var right = GetDirectionModel(MoveDirection.Right);

        if (!left.canMove && _inputVector.x == -1)
            _inputVector.x = 0;
        else if (!right.canMove && _inputVector.x == 1)
            _inputVector.x = 0;

        //ForceStopAbility = !left.canMove && !right.canMove;

        playerController.HorizontalDirection = _velocityVector.x;

        if (IsVelocityChanged())
        {
            bool value = _velocityVector.x > 0 ? false : true;

            if (value)
                transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);
            else
                transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);

            OnFlipX.Invoke(value);
        }

        if (_inputVector.x == 0)
        {
            _velocityVector.x = 0;
        }

        animator.SetBool(_upParameter, _inputVector.y > 0);
        animator.SetBool(_downParameter,_inputVector.y < 0);

        if (!playerController.AbilityJump.IsJumping && !playerController.AbilityJump.IsLanding)
            animator.SetFloat(_movementParameter, _velocityVector.x != 0 ? 1 : -1);
        else
            animator.SetFloat(_movementParameter, -1);
    }

    private bool CheckWall()
    {
        var hitCenter = Physics2D.Raycast(playerController.ColliderCenterPosition,
                                    new Vector3(transform.localScale.x, 0, 0), _castDistanceChecker, playerController.GroundLayers);
        var hitUp = Physics2D.Raycast(playerController.ColliderTopPosition,
                                    new Vector3(transform.localScale.x, 0, 0), _castDistanceChecker, playerController.GroundLayers);
        var hitDown = Physics2D.Raycast(playerController.ColliderBottomPosition + new Vector3(0,0.01f,0),
                                    new Vector3(transform.localScale.x, 0, 0), _castDistanceChecker, playerController.GroundLayers);

        if (hitCenter.collider == null &&
            hitUp.collider == null && 
            hitDown.collider == null) return false;
        
        return true;
    }

    private void OnDrawGizmos()
    {
        if (playerController == null) return;

        Debug.DrawRay(playerController.ColliderCenterPosition, new Vector3(transform.localScale.x * _castDistanceChecker, 0, 0), Color.red);
        Debug.DrawRay(playerController.ColliderTopPosition, new Vector3(transform.localScale.x * _castDistanceChecker, 0, 0), Color.red);
        Debug.DrawRay(playerController.ColliderBottomPosition + new Vector3(0, 0.01f, 0), new Vector3(transform.localScale.x * _castDistanceChecker, 0, 0), Color.red);
    }

    public void AllowDirection(MoveDirection moveDirection)
    {
        _permittedDirection.Find(d => d.permittedDirection == moveDirection).canMove = true;
    }

    public void DisallowDirection(MoveDirection moveDirection)
    {
        _permittedDirection.Find(d => d.permittedDirection == moveDirection).canMove = false;
    }

    public DirectionModel GetDirectionModel(MoveDirection moveDirection)
    {
        return _permittedDirection.Find(d => d.permittedDirection == moveDirection);
    }

    private bool IsVelocityChanged()
    {
        return _velocityVector.x != 0 && velTemp.x != _velocityVector.x;
    }
}
