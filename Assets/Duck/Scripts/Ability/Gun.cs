﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Gun : BaseAbility
{
    public System.Action<float> OnHeatTimeChanged;

    public bool IsFiring => _isFiring;

    public float TimeRemaing { get => _timeRemaing; set { _timeRemaing = value; OnHeatTimeChanged?.Invoke(_timeRemaing); } }
    public float MaxConsecutiveFireTime => _maxConsecutiveFireTime;
    
    [SerializeField]
    private bool _isAi = false;

    [Header("Fire Settings")]
    [SerializeField]
    private float _maxConsecutiveFireTime;
    [SerializeField]
    private Transform _bulletOrigin;
    [SerializeField]
    private GameObject _bulletPrefab;
    [SerializeField]
    private float _fireEvery;
    [SerializeField]
    private float _enableCommandsAfter = 0.2f;

    [Header("Pullback Settings")]
    [SerializeField]
    private float _force;
    [SerializeField]
    private float _linearDrag = 0.15f;

    [Header("Animations Settings")]
    [SerializeField]
    private string _firingParameter;

    [Header("Feedbacks")]
    [SerializeField]
    private ShootScreenShakeFeedback _shakeFeedback;
    [SerializeField]
    private VibrationFeedback _vibrationFeedback;
    [SerializeField]
    private SfxFeedback _sfxFeedback;
    [SerializeField]
    private GameObject _fireFeedback;

    [Header("Debug")]
    [SerializeField]
    private bool _isHeat;
    [SerializeField]
    private float _timeRemaing;
    [SerializeField]
    private PlayerController _player;
    private bool _isButtonPressed;
    private bool _isButtonHold;
    //[SerializeField]
    //private bool _isHeatingStarted;
    [SerializeField]
    private bool _isStopHeatingStarted;
    [SerializeField]
    private bool _canShoot;
    [SerializeField]
    private bool _isFiring;
    private WaitForEndOfFrame _waitFrame;

    protected override void Start()
    {
        _canShoot = true;

        CanPerformAbility = true;

        if (_player == null)
            _player = transform?.parent?.gameObject.GetComponent<PlayerController>();

        base.Start();
    }

    protected override void AbilityFixedUpdate()
    {

    }

    protected override void AbilityUpdate()
    {
        if (_isAi || !CanPerformAbility) return;

        if (ForceStopAbility) return;

        RotateWeapon();

        if (_isHeat)
        {
            _isFiring = false;
            animator.SetBool(_firingParameter, false);
            StartCoroutine(StopHeat());
            return;
        }

        if (inputManager.IsFireButtonPressed(0))
        {
            _isButtonPressed = true;

            Shoot();

            //StartCoroutine(StartHeat());
        }
        else if (inputManager.IsFireButtonHold(0))
        {
            _isButtonHold = true;

            Shoot();
        }
        if (inputManager.IsFireButtonReleased(0))
        {
            NotFiring();
        }

        if (_isFiring && !inputManager.IsFireButtonHold(0) && !inputManager.IsFireButtonPressed(0))
            NotFiring();
    }

    private void NotFiring()
    {
        _isButtonPressed = false;
        _isButtonHold = false;

        //if (_isAi)
        _isFiring = false;
        if (!_isAi)
            StartCoroutine(FiringRoutineTime());

        animator.SetBool(_firingParameter, false);
        StartCoroutine(StopHeat());
    }

    private IEnumerator FiringRoutineTime()
    {
        yield return new WaitForSeconds(_enableCommandsAfter);

        if (_isButtonHold || _isButtonPressed) yield break;

        _isFiring = false;
    }

    private void RotateWeapon()
    {
        Debug.Log($"Input vector {_player.HorizontalMove.InputVector}");

        float x = _player.HorizontalMove.InputVector.x;
        float y = _player.HorizontalMove.InputVector.y;

        if(y == 1)
        {
            transform.parent.localRotation = Quaternion.Euler(0, 0, 90);
        }
        else if(y == -1)
        {
            transform.parent.localRotation = Quaternion.Euler(0, 0, -90);
        }
        else if(x == 0 || x == -1)
        {
            transform.parent.localRotation = Quaternion.Euler(0, 0, 0);
        }
    }

    public void Shoot()
    {
        if (_isHeat) return;
        if (!_canShoot) return;

        _shakeFeedback?.Play(null, null);
        _vibrationFeedback?.Play(null,null);
        _sfxFeedback?.Play(null, null);
        Instantiate(_fireFeedback,_bulletOrigin.position,Quaternion.identity,null);

        _isFiring = true;

        StartCoroutine(WaitFire());

        animator.SetBool(_firingParameter, true);

        var randomizedPosition = new Vector3(_bulletOrigin.position.x, _bulletOrigin.position.y + Random.Range(-0.025f, 0.02f), _bulletOrigin.position.z);

        var bulletGo = Instantiate(_bulletPrefab,randomizedPosition,Quaternion.identity,null);

        var bullet = bulletGo.GetComponent<Bullet>();

        Vector2 direction = Vector2.zero;
        
        if(!_isAi)
            direction = _player.HorizontalMove.InputVector;

        if (direction == Vector2.zero && !_isAi)
            direction = new Vector2(_player.HorizontalMove.transform.localScale.x, 0);
        else if(_isAi)
            direction = new Vector2(_player.transform.localScale.x,0);

        Debug.DrawRay(transform.position, direction * _force, Color.black);

        bullet?.Initialize(direction,transform.parent.localEulerAngles.z);

        if (_player != null && !_isAi)
            MovePlayerInOppositeDirection(-direction);

        TimeRemaing += _fireEvery;

        if (TimeRemaing >= _maxConsecutiveFireTime)
        {
            _isHeat = true;

            if(!_isAi) return;

            StopHeatAI();
        }
    }

    public void StopHeatAI()
    {
        if (!_isAi) return;

        StartCoroutine(StopHeat());
    }

    private void MovePlayerInOppositeDirection(Vector3 direction)
    {
        _player.RBCharacter.gravityScale = 0.15f;
        _player.RBCharacter.drag = _linearDrag;
        _player.RBCharacter.AddForce(direction.normalized * _force,ForceMode2D.Impulse);
        _player.RBCharacter.velocity = new Vector2(Mathf.Clamp(_player.RBCharacter.velocity.x,-2,2), Mathf.Clamp(_player.RBCharacter.velocity.y, -2, 5));
    }

    private IEnumerator WaitFire()
    {
        _canShoot = false;

        yield return new WaitForSeconds(_fireEvery);

        _canShoot = true;
    }

    private IEnumerator StopHeat()
    {
        if (_isStopHeatingStarted) yield break;
        
        _isStopHeatingStarted = true;

        yield return _waitFrame;

        if (_isHeat)
        {
            while (_isHeat)
            {
                while (DuckGameManager.Instance.CurrentState == GameState.InPause || 
                       DuckGameManager.Instance.CurrentState == GameState.InRoomTransition || 
                       DuckGameManager.Instance.CurrentState == GameState.InPause)
                {
                    yield return _waitFrame;
                }

                TimeRemaing -= _fireEvery;

                Debug.Log($"Time remaining {_timeRemaing}");

                if(TimeRemaing <= 0)
                {
                    TimeRemaing = 0;
                    _isHeat = false;
                    _isStopHeatingStarted = false;
                    yield break;
                }

                yield return new WaitForSeconds(_fireEvery);
            }

            _isStopHeatingStarted = false;
        }
        else
        {
            while (TimeRemaing > 0 && (!_isButtonPressed || !_isButtonHold))
            {
                while (DuckGameManager.Instance.CurrentState == GameState.InPause ||
                       DuckGameManager.Instance.CurrentState == GameState.InRoomTransition ||
                       DuckGameManager.Instance.CurrentState == GameState.InPause)
                {
                    yield return _waitFrame;
                }

                TimeRemaing -= _fireEvery;

                if (TimeRemaing <= 0)
                {
                    TimeRemaing = 0;
                    _isStopHeatingStarted = false;
                    yield break;
                }

                yield return new WaitForSeconds(_fireEvery);
            }

            _isStopHeatingStarted = false;
        }
    }
}
