﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseAbility : MonoBehaviour
{
	public bool CanPerformAbility { get; set; }

	public bool ForceStopAbility { get; set; }

	[SerializeField]
	protected Animator animator;
	[SerializeField]
	protected SpriteRenderer spriteRenderer;

	protected InputManager inputManager;
	protected PlayerController playerController;

	protected BoxCollider _boxCollider;

	protected virtual void Start()
    {
		inputManager = InputManager.Instance;
		playerController = GetComponent<PlayerController>();

		animator = GetComponent<Animator>();
		spriteRenderer = GetComponent<SpriteRenderer>();

		DuckGameManager.Instance.OnUpdate += AbilityUpdate;
		DuckGameManager.Instance.OnFixedUpdate += AbilityFixedUpdate;
    }
	
	protected abstract void AbilityUpdate();

	protected abstract void AbilityFixedUpdate();

	protected virtual void OnDestroy()
    {
		DuckGameManager.Instance.OnUpdate -= AbilityUpdate;
		DuckGameManager.Instance.OnFixedUpdate -= AbilityFixedUpdate;
	}
}
