﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerType
{
    AI,
    Player
}

public class PlayerController : MonoBehaviour
{
    #region Properties

    public PlayerType PlayerType => _playerType;
    public float HorizontalDirection { get => _horizontalDirection; set => _horizontalDirection = value; }
    public Rigidbody2D RBCharacter => _rbCharacter;
    public bool IsGrounded => _isGrounded;
    public LayerMask GroundLayers => _groundLayers;
    public Vector3 ColliderSize => Vector3.Scale(transform.localScale, _collider.bounds.size);
    public Vector3 ColliderCenterPosition => _collider.bounds.center;
    public virtual Vector3 ColliderBottomPosition => new Vector3(_collider.bounds.center.x, _collider.bounds.min.y, 0);
    public virtual Vector3 ColliderLeftPosition => new Vector3(_collider.bounds.min.x, _collider.bounds.center.y, 0);
    public virtual Vector3 ColliderTopPosition => new Vector3(_collider.bounds.center.x, _collider.bounds.max.y, 0);
    public virtual Vector3 ColliderRightPosition => new Vector3(_collider.bounds.max.x, _collider.bounds.center.y, _collider.bounds.max.x);

    #endregion

    #region PlayerProperties
    public Jump AbilityJump => _abilityJump;
    public HorizontalMove HorizontalMove => _horizontalMove;
    public Gun GunAbility => _gunAbility;

    #endregion

    #region AIProperties

    #endregion

    public SfxFeedback SfxFeedback;

    [SerializeField]
    private Rigidbody2D _rbCharacter;
    [SerializeField]
    private Collider2D _collider;

    [Header("Core")]
    [SerializeField]
    private PlayerType _playerType;

    [Header("Grounded Attributes")]
    [SerializeField]
    private float _radiusCheck;
    [SerializeField]
    private bool _isGrounded;
    [SerializeField]
    private LayerMask _groundLayers;
    [SerializeField]
    private float _checkerOffset = 0.3f;

    [Header("Player Abilities")]
    [SerializeField]
    private Jump _abilityJump;
    [SerializeField]
    private HorizontalMove _horizontalMove;
    [SerializeField]
    private Gun _gunAbility;

    [Header("AI Abilities")]
    [SerializeField]
    private Brain _brain;

    private Vector2 _velocityBeforePause;
    private float _horizontalDirection;

    private BaseAbility[] _abilities;

    private void Awake()
    {
        if (_playerType == PlayerType.AI)
            InitAI();
        else
            InitiPlayer();

        _abilities = GetComponents<BaseAbility>();

        DamagableComponent dc = GetComponent<DamagableComponent>();
        if (dc != null)
        {
            dc.OnDeath.AddListener(DieEffect);
        }

        foreach (var ability in _abilities)
        {
            ability.CanPerformAbility = true;
        }

        DuckGameManager.Instance.OnGameStateChanged += StateChanged;
        DuckGameManager.Instance.OnUpdate += PlayerUpdate;
        DuckGameManager.Instance.OnFixedUpdate += PlayerFixedUpdate;
    }

    private void InitiPlayer()
    {
        _horizontalMove = GetComponent<HorizontalMove>();
        _abilityJump = GetComponent<Jump>();
    }

    private void InitAI()
    {
        _brain = GetComponent<Brain>();
    }

    private void StateChanged(GameState state)
    {
        if (state == GameState.InGame)
            Unfreeze();
        else
            Freeze();
    }

    public void Freeze()
    {
        if (RBCharacter != null)
        {
            _velocityBeforePause = RBCharacter.velocity;
            RBCharacter.constraints = RigidbodyConstraints2D.FreezeAll;
        }
        foreach (var ability in _abilities)
        {
            if (ability == null) continue;

            ability.CanPerformAbility = false;
        }
    }

    public void Unfreeze()
    {
        if (RBCharacter != null)
        {
            RBCharacter.constraints = RigidbodyConstraints2D.None;
            RBCharacter.constraints = RigidbodyConstraints2D.FreezeRotation;
            RBCharacter.velocity = _velocityBeforePause;
        }
        foreach (var ability in GetComponents<BaseAbility>())
        {
            if (ability == null) continue;

            ability.CanPerformAbility = true;
        }
    }

    private void PlayerUpdate()
    {

    }

    public void DieEffect(DamagableComponent dc)
    {
        SfxFeedback?.Play(null, null);
    }

    private void PlayerFixedUpdate()
    {
        if (_collider == null) return;

        _isGrounded = Physics2D.OverlapCircle(new Vector3(ColliderBottomPosition.x, ColliderBottomPosition.y + _checkerOffset, ColliderBottomPosition.z), _radiusCheck, _groundLayers);

        if (PlayerType == PlayerType.AI) return;

        if (!AbilityJump.IsJumping && !GunAbility.IsFiring && _isGrounded)
            RBCharacter.velocity = new Vector2(RBCharacter.velocity.x,0);
    }

    private void OnDestroy()
    {
        DuckGameManager.Instance.OnGameStateChanged -= StateChanged;
        DuckGameManager.Instance.OnUpdate -= PlayerUpdate;
        DuckGameManager.Instance.OnFixedUpdate -= PlayerFixedUpdate;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (PlayerType == PlayerType.AI) return;

        var check = collision.gameObject.GetComponent<Checkpoint>();

        if (check == null || check.isPassed) return;

        check.isPassed = true;

        DuckGameManager.Instance.SetCheckpoint(check);

        Debug.Log($"Collision trigger {collision.gameObject.name}");
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (_collider == null) return;

        Gizmos.color = Color.yellow;

        Gizmos.DrawSphere(new Vector3(ColliderBottomPosition.x, ColliderBottomPosition.y + _checkerOffset, ColliderBottomPosition.z), _radiusCheck);
    }
#endif
}
