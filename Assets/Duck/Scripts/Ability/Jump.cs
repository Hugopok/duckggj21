﻿using System.Collections;
using UnityEngine;

public class Jump : BaseAbility
{
    public bool IsLanding => _isLanding;
    public bool IsJumping => _isJumping;
    public bool CanPerformCoyote => _canPerformCoyote;

    public SfxFeedback sfxFeedback;

    [Header("Vertical Movement")]
    public float jumpForce = 15f;
    private float jumpTimeCounter;
    [SerializeField]
    private float jumpTime;

    [Header("Coyote Settings")]
    [SerializeField]
    private float _coyoteTimer = 0.14f;
    [SerializeField]
    private bool _canPerformCoyote = false;

    [Header("Gravity settings")]
    [SerializeField]
    private float _linearDrag;
    [SerializeField]
    private float _gravity = 1f;
    [SerializeField]
    private float _fallMultiplier = 5f;

    [Header("Animations Settings")]
    [SerializeField]
    private string _jumpingParameter;
    [SerializeField]
    private string _landingParameter;
    [Header("Debug")]
    [SerializeField]
    private bool _isJumping = false;
    [SerializeField]
    private bool _isLanding = false;

    private bool _wasOnGround = false;

    private bool _isButtonHold;
    private bool _isButtonPressed;
    private float _originalLinearDrag;

    protected override void Start()
    {
        base.Start();

         _originalLinearDrag = playerController.RBCharacter.drag;
    }

    protected override void AbilityFixedUpdate()
    {
        ModifyPhysics();
    }

    protected override void AbilityUpdate()
    {
        // Take Input
        _isButtonHold = inputManager.IsJumpButtonHold(0);
        _isButtonPressed = inputManager.IsJumpButtonPressed(0);

        if (_isButtonPressed && !playerController.IsGrounded && !CanPerformCoyote) return;

        if (!_isJumping && _wasOnGround && !playerController.IsGrounded)
        {
            StartCoroutine(PerformCoyote());
        }

        _wasOnGround = playerController.IsGrounded;

        if (CanJump() && _isButtonPressed)
        {
            JumpFunction();
        }
        else if (_isJumping && _isButtonHold)
        {
            if (jumpTimeCounter > 0)
            {
                playerController.RBCharacter.velocity = new Vector2(playerController.RBCharacter.velocity.x, jumpForce);
                jumpTimeCounter -= Time.deltaTime;
            }
            else
            {
                Debug.Log($"Pos {transform.position}");
                _isJumping = false;
            }
        }
        if (inputManager.IsJumpButtonReleased(0))
        {
            jumpTimeCounter = 0;
            _isJumping = false;
            playerController.RBCharacter.velocity = new Vector2(playerController.RBCharacter.velocity.x, 0);
        }

        if (CheckState() == _landingParameter && !playerController.GunAbility.IsFiring)
        {
            if (playerController.IsGrounded)
            {
                if(!inputManager.IsJumpButtonHold(0) || !inputManager.IsJumpButtonPressed(0))
                {
                    _isJumping = false;
                    _isLanding = true;
                    animator.SetBool(_landingParameter, false);
                    animator.SetBool(_jumpingParameter, false);
                }

                return;

            }

            Debug.Log("Landing");

            _isJumping = false;
            _isLanding = true;
            animator.SetBool(_landingParameter, true);
            animator.SetBool(_jumpingParameter, false);
        }
        else if (string.IsNullOrEmpty(CheckState()))
        {
            _isJumping = false;
            _isLanding = false;
            animator.SetBool(_landingParameter, false);
            animator.SetBool(_jumpingParameter, false);
        }

        if(playerController.IsGrounded && (!inputManager.IsJumpButtonPressed(0) && !inputManager.IsJumpButtonHold(0)))
        {
            _isJumping = false;
            _isLanding = false;
            animator.SetBool(_landingParameter, false);
            animator.SetBool(_jumpingParameter, false);
        }

    }

    private void SetAnimator(string param, bool value)
    {
        animator?.SetBool(param,value);
    }

    private bool CanJump()
    {
        return (playerController.IsGrounded || _canPerformCoyote);
    }

    private IEnumerator PerformCoyote()
    {
        _canPerformCoyote = true;

        yield return new WaitForSeconds(_coyoteTimer);

        _canPerformCoyote = false;
    }

    private void JumpFunction()
    {
        if (ForceStopAbility) return;

        if (CanPerformCoyote)
            playerController.RBCharacter.velocity = new Vector2(playerController.RBCharacter.velocity.x,0);

        jumpTimeCounter = jumpTime;

        playerController.RBCharacter.velocity = new Vector2(playerController.RBCharacter.velocity.x, jumpForce);

        animator.SetBool(_landingParameter, false);
        animator.SetBool(_jumpingParameter, true);
        
        _isLanding = false;
        _isJumping = true;

        sfxFeedback?.Play(null, null);
    }

    private void ModifyPhysics()
    {
        if (playerController.GunAbility.IsFiring) return;

        if (playerController.HorizontalMove.ForceDown)
        {
            playerController.RBCharacter.gravityScale = 1f;
            return;
        }

        if(!playerController.IsGrounded)
        {
            if(!playerController.GunAbility.IsFiring)
                playerController.RBCharacter.gravityScale = _gravity;
            
            playerController.RBCharacter.drag = _linearDrag * 0.15f;
            
            if (playerController.RBCharacter.velocity.y < 0 && !playerController.GunAbility.IsFiring && !CanPerformCoyote)
            {
                playerController.RBCharacter.gravityScale = _gravity * _fallMultiplier;
            }
            else if (playerController.RBCharacter.velocity.y > 0 && !inputManager.IsJumpButtonHold(0) && !playerController.GunAbility.IsFiring)
            {
                playerController.RBCharacter.gravityScale = _gravity * (_fallMultiplier / 2);
            }
        }
        else if(playerController.IsGrounded && !playerController.GunAbility.IsFiring)
            playerController.RBCharacter.drag = _originalLinearDrag;
    }

    private string CheckState()
    {
        if (playerController.RBCharacter.velocity.y > 0) return _jumpingParameter;

        if (playerController.RBCharacter.velocity.y < 0 && !playerController.IsGrounded) return _landingParameter;

        return string.Empty;
    }
}
