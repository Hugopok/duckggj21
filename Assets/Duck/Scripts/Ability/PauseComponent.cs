﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseComponent : BaseAbility
{
    protected override void Start()
    {
        base.Start();

        DuckGameManager.Instance.OnGameStateChanged += StateChanged;
    }

    void StateChanged(GameState state)
    {
        if (state == GameState.InPause)
            inputManager.ChangeMapToUI();
        else if (state == GameState.InGame)
            inputManager.ChangeMapToDefault();
    }

    protected override void AbilityFixedUpdate()
    {
        
    }

    protected override void AbilityUpdate()
    {

    }

    private void Update()
    {
        if (inputManager.IsPauseButtonPressed(0))
        {
            DuckGameManager.Instance.SwapPause();

           
        }
    }
}
