﻿using System;
using System.Collections;
using UnityEngine;

public enum DeathConditions
{
    DeathOnZero,
    DeathMinusZero
}

[Serializable]
public class DamagableComponent : MonoBehaviour
{
    public DamagableEvent OnDamage = new DamagableEvent();
    public DamagableEvent OnAddHealth = new DamagableEvent();
    public DamagableEvent OnDeath = new DamagableEvent();

    public float MaxHealth => maxHealth;
    public float Health { get => health; set => health = value; }
    public bool IsInvincible => invulnerabilityTime > 0 && isInvulnerable;
    public bool IsPerforming => isPerforming;
    public bool GodMode => godMode;
    public float InvulnerableTime => invulnerabilityTime;
    public bool IsDeath { get; private set; }

    [Header("Settings")]
    [SerializeField] protected bool godMode;
    [SerializeField] protected float health;
    [SerializeField] protected float maxHealth;
    [SerializeField] protected DeathConditions deathConditions;
    [SerializeField] protected float invulnerabilityTime;
    [SerializeField] protected bool destroyOnZero;

    [Space(10)]
    [Header("Feedbacks")]

    [SerializeField] protected SfxFeedback deathSound;
    [SerializeField] protected AFeedback hitFeedback;
    [SerializeField] protected AFeedback hitForceFeedback;

    protected bool isInvulnerable;
    protected bool isPerforming;

    public virtual void Damage(ContactPoint contactPoint, float damage)
    {
        if (IsDeath || GodMode) return;

        hitForceFeedback?.Play(null, -contactPoint.normal);

        Damage(damage);
    }

    public virtual void Damage(RaycastHit contactPoint, float damage)
    {
        if (IsDeath || GodMode) return;

        hitForceFeedback?.Play(null, -contactPoint.normal);

        Damage(damage);
        //photonView.RPC("Damage", RpcTarget.MasterClient, damage);
    }

    public virtual void Damage(RaycastHit2D contactPoint, float damage)
    {
        if (IsDeath || GodMode) return;

        hitForceFeedback?.Play(null, -contactPoint.normal);

        Damage(damage);
        //photonView.RPC("Damage", RpcTarget.MasterClient, damage);
    }

    public virtual void SetInvincible()
    {
        godMode = true;
    }

    public virtual void RemoveInvincible()
    {
        godMode = false;
    }

    public virtual void DamageWithNoEffects(float damage)
    {
        health -= damage;
    }

    protected virtual void Damage(float damage)
    {
        if (godMode || isInvulnerable)
        {
            Debug.Log("Non posso danneggiare, perchè è invulnerabile");
            return;
        }

        if (deathConditions == DeathConditions.DeathOnZero && health <= 0) return;

        if (isPerforming)
        {
            Debug.Log($"Is already performing {isPerforming} : {gameObject.name}");
            return;
        }

        isPerforming = true;

        if (invulnerabilityTime > 0)
        {
            StartCoroutine(InvulnerabilityTime(invulnerabilityTime));
        }

       // if (!isInvulnerable)
       ApplyDamage(damage);

        if (deathConditions == DeathConditions.DeathMinusZero)
        {
            if (health < 0)
                ApplyDeath();
            else
                ApplyHitFeedback();
        }
        else
        {
            if (health == 0)
                ApplyDeath();
            else
                ApplyHitFeedback();
        }

        if (invulnerabilityTime == 0)
            isPerforming = false;
    }

    public virtual void AddHealth(float healthToAdd)
    {
        if (health < maxHealth)
        {
            health += healthToAdd;
            OnAddHealth?.Invoke(this);
        }
    }

    public virtual void Revive()
    {
        IsDeath = false;
        deathSound?.Play(gameObject,null);
        health = maxHealth;
    }

    protected virtual void ApplyDeath()
    {
        deathSound?.Play(gameObject,null);

        IsDeath = true;

        OnDeath?.Invoke(this);

        if (destroyOnZero)
            Destroy(gameObject);
    }

    protected virtual void ApplyDamage(float damage)
    {
        health -= damage;
        OnDamage?.Invoke(this);
    }

    protected virtual void ApplyHitFeedback()
    {
        hitFeedback?.Play(gameObject, invulnerabilityTime);
    }

    protected virtual IEnumerator InvulnerabilityTime(float time)
    {
        isInvulnerable = true;

        yield return new WaitForSeconds(time);

        isInvulnerable = false;
        isPerforming = false;
    }
}
