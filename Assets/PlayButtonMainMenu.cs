﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayButtonMainMenu : MonoBehaviour, ISelectHandler, 
                                                 IDeselectHandler,
                                                 IPointerEnterHandler, 
                                                 IPointerExitHandler
{
    public GameObject arrow;

    public void OnDeselect(BaseEventData eventData)
    {
        arrow.SetActive(false);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        arrow.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        arrow.SetActive(false);
    }

    public void OnSelect(BaseEventData eventData)
    {
        arrow.SetActive(true);
    }
}
